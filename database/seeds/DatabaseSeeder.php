<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Seeding default data of Users' . PHP_EOL);
        $this->call(UsersTableSeeder::class);

        $this->command->info('Seeding default data of User Profiles' . PHP_EOL);
        $this->call(UserProfilesTableSeeder::class);

        $this->command->info('Seeding default data of Events' . PHP_EOL);
        $this->call(EventsTableSeeder::class);

        // $this->command->info('Seeding default data of Players' . PHP_EOL);
        // $this->call(PlayersTableSeeder::class);

        // DB::statement("INSERT INTO `prizes` (`id`, `event_id`, `particulars`, `prize_type`, `branch`, `slug`, `quantity`, `claimed_count`, `unit_cost`, `total_price`, `created_at`, `updated_at`) VALUES
        // (1, 1, 'Rice Cooker', 'Consolation Prize', 'Naguilian Branch', 'rice-cooker-naguilian-branch-branch', '3', 0, NULL, NULL, '2020-11-04 01:26:49', '2020-11-04 01:28:21'),
        // (2, 1, 'Rice Cooker', 'Consolation Prize', 'Roxas Branch', 'rice-cooker-roxas-branch-branch', '3', 0, NULL, NULL, '2020-11-04 01:27:03', '2020-11-04 01:28:16'),
        // (3, 1, 'Rice Cooker', 'Consolation Prize', 'Alicia Branch', 'rice-cooker-alicia-branch-branch', '3', 0, NULL, NULL, '2020-11-04 01:27:25', '2020-11-04 01:28:12'),
        // (4, 1, 'Rice Cooker', 'Consolation Prize', 'Solano Branch', 'rice-cooker-solano-branch-branch', '3', 0, NULL, NULL, '2020-11-04 01:27:30', '2020-11-04 01:28:07'),
        // (5, 1, 'Rice Cooker', 'Consolation Prize', 'Cabarroguis Branch', 'rice-cooker-cabarroguis-branch-branch', '3', 0, NULL, NULL, '2020-11-04 01:27:36', '2020-11-04 01:28:02'),
        // (6, 1, 'Rice Cooker', 'Consolation Prize', 'Tuguegarao Branch', 'rice-cooker-tuguegarao-branch-branch', '3', 0, NULL, NULL, '2020-11-04 01:27:42', '2020-11-04 01:27:57'),
        // (7, 1, 'Realme 5i', 'Consolation Prize', 'Naguilian Branch', 'realme-5i-naguilian-branch', '1', 0, NULL, NULL, '2020-11-04 01:28:45', '2020-11-04 01:28:45'),
        // (8, 1, 'Realme 5i', 'Consolation Prize', 'Roxas Branch', 'realme-5i-roxas-branch', '1', 0, NULL, NULL, '2020-11-04 01:28:51', '2020-11-04 01:28:51'),
        // (9, 1, 'Realme 5i', 'Consolation Prize', 'Alicia Branch', 'realme-5i-alicia-branch', '1', 0, NULL, NULL, '2020-11-04 01:28:56', '2020-11-04 01:28:56'),
        // (10, 1, 'Realme 5i', 'Consolation Prize', 'Solano Branch', 'realme-5i-solano-branch', '1', 0, NULL, NULL, '2020-11-04 01:29:00', '2020-11-04 01:29:00'),
        // (11, 1, 'Realme 5i', 'Consolation Prize', 'Cabarroguis Branch', 'realme-5i-cabarroguis-branch', '1', 0, NULL, NULL, '2020-11-04 01:29:05', '2020-11-04 01:29:05'),
        // (12, 1, 'Realme 5i', 'Consolation Prize', 'Tuguegarao Branch', 'realme-5i-tuguegarao-branch', '1', 0, NULL, NULL, '2020-11-04 01:29:11', '2020-11-04 01:29:11'),
        // (13, 1, '40 inches Xtreme LED TV with Cignal Kit', 'Consolation Prize', 'Naguilian Branch', '40-inches-xtreme-led-tv-with-cignal-kit-naguilian-branch-branch', '1', 0, NULL, NULL, '2020-11-04 01:35:10', '2020-11-04 01:35:42'),
        // (14, 1, '40 inches Xtreme LED TV with Cignal Kit', 'Consolation Prize', 'Roxas Branch', '40-inches-xtreme-led-tv-with-cignal-kit-roxas-branch', '1', 0, NULL, NULL, '2020-11-04 01:36:44', '2020-11-04 01:36:44'),
        // (15, 1, '40 inches Xtreme LED TV with Cignal Kit', 'Consolation Prize', 'Alicia Branch', '40-inches-xtreme-led-tv-with-cignal-kit-alicia-branch', '1', 0, NULL, NULL, '2020-11-04 01:36:51', '2020-11-04 01:36:51'),
        // (16, 1, '40 inches Xtreme LED TV with Cignal Kit', 'Consolation Prize', 'Solano Branch', '40-inches-xtreme-led-tv-with-cignal-kit-solano-branch', '1', 0, NULL, NULL, '2020-11-04 01:36:55', '2020-11-04 01:36:55'),
        // (17, 1, '40 inches Xtreme LED TV with Cignal Kit', 'Consolation Prize', 'Cabarroguis Branch', '40-inches-xtreme-led-tv-with-cignal-kit-cabarroguis-branch', '1', 0, NULL, NULL, '2020-11-04 01:37:00', '2020-11-04 01:37:00'),
        // (18, 1, '40 inches Xtreme LED TV with Cignal Kit', 'Consolation Prize', 'Tuguegarao Branch', '40-inches-xtreme-led-tv-with-cignal-kit-tuguegarao-branch', '1', 0, NULL, NULL, '2020-11-04 01:37:05', '2020-11-04 01:37:05'),
        // (19, 1, 'Gift Certificate worth 30,000.00 of PMPC Products', 'Major Prize', 'All', 'gift-certificate-worth-3000000-of-pmpc-products-all-branch', '1', 0, NULL, NULL, '2020-11-04 01:38:53', '2020-11-04 01:39:02'),
        // (20, 1, 'Gift Certificate worth 40,000.00 of PMPC Products', 'Major Prize', 'All', 'gift-certificate-worth-4000000-of-pmpc-products-all', '1', 0, NULL, NULL, '2020-11-04 01:39:07', '2020-11-04 01:39:07'),
        // (21, 1, 'Gift Certificate worth 50,000.00 of PMPC Products', 'Major Prize', 'All', 'gift-certificate-worth-5000000-of-pmpc-products-all', '1', 0, NULL, NULL, '2020-11-04 01:39:11', '2020-11-04 01:39:11');");
    }
}
