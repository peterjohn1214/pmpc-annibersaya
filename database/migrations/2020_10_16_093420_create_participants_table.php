<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
            $table->string('date_purchased')->nullable();
            $table->string('full_name');
            $table->string('branch')->nullable();
            $table->string('address')->nullable();
            $table->string('ticket_number')->nullable();

            // $table->bigInteger('member_id')->unsigned();
            // $table->foreign('member_id')->references('id')->on('members');
            // $table->tinyInteger('major_winner')->default(0);
            // $table->tinyInteger('consolation_winner')->default(0);
            $table->timestamps();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('deleted_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
