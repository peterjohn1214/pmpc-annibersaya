<div class="uis-modal" id="close-event">
    <div class="uis-modal-dialog">
        <div class="uis-modal-body">
            <h2 class="uis-modal-title">Are you sure</h2>

            <p>Do you want to <span id="js-status-event-active"></span> the <span class="uis-text-primary"
                    id="js-status-event-name"></span> @{{ event.is_active ? 'Close' : 'Open'}} this event?</p>
        </div>

        <div class="uis-modal-footer uis-text-right">
            <button class="uis-button uis-button-danger" v-on:click="changeStatus">Yes</button>
            <button class="uis-button" type="button" uis-modal="#close-event">Cancel</button>
        </div>
    </div>
</div>