<?php

namespace App\Http\Controllers\API\Administration;

use Excel;
use App\Ticket;
use App\Participant;
use App\ParticipantExcel;
use Illuminate\Http\Request;
use App\Imports\ParticipantImport;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ImportExcelController extends Controller
{
    function index(Request $request)
    {
        $this->validate($request, [
            'import_file' => 'required|mimes:xls,xlsx'
        ]);

        $data = Excel::toCollection(new ParticipantImport, request()->file('import_file'));
        if (count($data[0]) > 0) {
            foreach ($data[0] as $row) {
                $listItems[] = array(
                    'date_purchased' => $row[0],
                    'full_name' => $row[1],
                    'address' => $row[2],
                    'branch' => $row[3],
                    'ticket_no' => $row[4],
                    'event_id' => $request->event_id,
                );
            }
        }

        return response()->json($listItems, 200);
    }

    function import(Request $rows)
    {
        ini_set('max_execution_time', 50000);
        $event_id = $rows[0]["event_id"];
        DB::beginTransaction();
        DB::statement('SET FOREIGN_KEY_CHECKS=OFF');
        // DB::delete('delete from participants where event_id = ?', [$event_id]);
        // DB::delete('delete from tickets where event_id = ?', [$event_id]);
        try {
            foreach ($rows->all() as $row) {
                $participant = new Participant();
                $participant->full_name = $row["full_name"];
                $participant->date_purchased = $row["date_purchased"];
                $participant->event_id = $row["event_id"];
                $participant->branch = $row["branch"];
                $participant->address = $row["address"];

                $participant->ticket_number = sprintf('%06d' . PHP_EOL, $row["ticket_no"]);

                $participant->save();

                // for ($i = 1; $i <=  $row["ticket_count"]; $i++) {
                //     $lastTicket = DB::table('tickets')
                //         ->where('event_id', '=', $event_id)
                //         ->orderBy('id', 'DESC')->first();

                //     $ticketId = $lastTicket == null ? 1 : (int) $lastTicket->ticket_no + 1;
                //     $ticket = new Ticket();
                //     $ticket->ticket_no = sprintf('%06d' . PHP_EOL, $ticketId);
                //     $ticket->event_id = $participant->event_id;
                //     $ticket->participant_id = $participant->id;
                //     $ticket->created_by = Auth::user()->id;

                //     $ticket->saveOrFail();
                // }
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'something went wrong',
                'details' => $th,
            ], 500);
        }

        DB::commit();

        return response()->json([
            'message' => 'Uploaded Successfully',
            'details' => '',
        ], 200);
    }
}
