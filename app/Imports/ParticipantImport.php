<?php

namespace App\Imports;

use App\Participant;
use App\ParticipantExcel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ParticipantImport implements ToModel, WithStartRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return ([
            'date_purchased' => $row[0],
            'full_name' => $row[1],
            'address' => $row[2],
            'branch' => $row[3],
            'ticket_no' => $row[4],
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
