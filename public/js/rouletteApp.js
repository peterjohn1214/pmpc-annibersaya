jQuery.noConflict();
jQuery(function ($) {
    let participants = '';
    let winner = '';
    let index = '';
    let winnerId = '';
    let eventId = document.getElementById("event-id").value;
    let prizeId = document.getElementById("prize-id").value;
    let branch = document.getElementById("branch").value;
    let prizeType = document.getElementById("prize-type").value;
    let remaining = document.getElementById("remaining");
    let rouletter = '';
    let rouletter2 = '';
    let rouletter3 = '';
    let rouletter4 = '';
    let rouletter5 = '';
    let rouletter6 = '';
    let rouletter7 = '';
    let rouletter8 = '';
    let s1, s2, s3, s4, s5, s6, s7, s8;
    let r1, r2, r3, r4, r5, r6, r7, r8;
    let currentPrize = null;


    let isRolling = false;

    console.log(remaining)


    $('.start').attr('disabled', 'true');

    $('body').keydown(function (e) {
        if (e.keyCode === 32) {
            if (participants.length >= 1 && currentPrize.remaining >= 1 && isRolling == false) {
                $("p").addClass("winner-name-hide");
                $(".winner-address").text("");
                confetti.maxCount = 0;
                confetti.stop();
                startRoulette();
            }
        }
    })


    r1 = {
        speed: 50,
        duration: 2,
        startCallback: function () {
            $('.start').attr('disabled', 'true');
            $(".winner-name").text("");
            $(".winner-address").text("");
            $('winner-name').css('display', 'none');

        },
        slowDownCallback: function () {

        },
        stopCallback: function () {

        }

    }

    r2 = {
        speed: 60,
        duration: 5,

        startCallback: function () {

        },
        slowDownCallback: function () {

        },
        stopCallback: function () {

        }

    }

    r3 = {
        speed: 70,
        duration: 8,

        startCallback: function () {

        },
        slowDownCallback: function () {

        },
        stopCallback: function () {

        }

    }

    r4 = {
        speed: 80,
        duration: 10,
        startCallback: function () {},
        slowDownCallback: function () {},
        stopCallback: function () {}
    }

    r5 = {
        speed: 90,
        duration: 12,
        startCallback: function () {},
        slowDownCallback: function () {},
    }



    r6 = {
        speed: 100,
        duration: 14,
        startCallback: function () {
            speed = -10;
        },
        slowDownCallback: function () {

        },
        stopCallback: function () {
            confetti.maxCount = 1000;
            setTimeout(() => {
                saveWinner();
                $(".winner-name").text(winner.full_name);
                // $(".winner-address").text(winner.address);
                confetti.start();
                $('.start').removeAttr('disabled');
            }, 500);

            // setTimeout(() => {
            //     confetti.stop();
            // }, 4000);

        }

    }

    // r7 = {
    //     speed: 105,
    //     duration: 9,
    //     startCallback: function () {},
    //     slowDownCallback: function () {

    //     },
    //     stopCallback: function () {
    //     }
    // }

    // r8 = {
    //     speed: 110,
    //     duration: 10,
    //     startCallback: function () {},

    //     stopCallback: function () {

    //     }

    // }

    function startRoulette() {
        isRolling = true;
        confetti.stop();
        console.log('not winners', participants);

        // var shuffledParticipants =  shuffle(participants);

       
        // index = Math.floor((Math.random() * participants.length) + 1);
        // winner = participants[index];
        // winnerId = Array.from(String(winner.ticket_number), Number);

        index = Math.floor((Math.random() * participants.length) + 1);
        winner = participants[index];
        winnerId = Array.from(String(winner.ticket_number), Number);

        console.log('winner',winner);

        // var ticketNo = randomTicketNo();

        
        updateParamater(winnerId);

        setTimeout(() => {
            rouletter.roulette('start');
        }, 80);
        rouletter2.roulette('start');
        setTimeout(() => {
            rouletter3.roulette('start');
        }, 100);
        setTimeout(() => {
            rouletter4.roulette('start');
        }, 120);
        setTimeout(() => {
            rouletter5.roulette('start');

        }, 150);

        setTimeout(() => {
            rouletter6.roulette('start');
        }, 300);

        // rouletter6.roulette('start');
        // rouletter7.roulette('start');
        // rouletter8.roulette('start');

    }

    var getPrizeDetail = function () {
        jQuery.get(`/api/administration/prizes/${prizeId}`, function (data) {
            currentPrize = data;
            console.log('currentPrize', currentPrize);
            remaining.innerText = `${currentPrize.claimed_count} of ${currentPrize.quantity} winners`;

             
        })
    }

    var getNotWinners = function () {
        $('.start').removeAttr('disabled');
        jQuery.get(`/api/administration/event/participants?event_id=${eventId}&branch=${branch}&prize_type=${prizeType}`, function (data) {
            participants = data;
            console.log(participants);
            if (participants.length >= 1 && currentPrize.remaining >= 1) {
                $('.start').removeAttr('disabled');
            } else {
                $('.start').attr('disabled', 'true');
            }
        });
    }

    function shuffle(array) {
        var currentIndex = array.length,  randomIndex;
      
        while (0 !== currentIndex) {
      
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex--;
      
          [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
        }
      
        return array;
    }


    var getRafflePrize = function () {
        jQuery.get("/currentPrize", function (data) {
            $("#raffle-prize-label").html(`${data.prize} - ${data.description}`);
        })
    }

    getPrizeDetail();
    getNotWinners();

    function saveWinner() {
        var data = {
            participant_id: winner.participant_id,
            participant_name: winner.full_name,
            ticket_no: winner.ticket_number,
            branch: winner.branch,
            prize_id: prizeId,
            event_id: eventId,
        }
        $.ajax({
            url: "/api/administration/event/winner",
            type: "post",
            data: data,
            success: function (data) {
                toastr.success(`${winner.full_name} successfully added to winners`, "Success");
                getPrizeDetail();
                getNotWinners();
            }
        });

        

        
    }

    var updateParamater = function (arrayNumber) {

        r1['stopImageNumber'] = Number(arrayNumber[0]);
        r2['stopImageNumber'] = Number(arrayNumber[1]);
        r3['stopImageNumber'] = Number(arrayNumber[2]);
        r4['stopImageNumber'] = Number(arrayNumber[3]);
        r5['stopImageNumber'] = Number(arrayNumber[4]);
        r6['stopImageNumber'] = Number(arrayNumber[5]);
        // r6['stopImageNumber'] = Number(arrayNumber[5]);
        // r7['stopImageNumber'] = Number(arrayNumber[6]);
        // r8['stopImageNumber'] = Number(arrayNumber[7]);

        // r1['duration'] = parseInt(s1);
        // r2['duration'] = parseInt(s2);
        // r3['duration'] = parseInt(s3);
        // r4['duration'] = parseInt(s4);

        rouletter.roulette('option', r1);
        rouletter2.roulette('option', r2);
        rouletter3.roulette('option', r3);
        rouletter4.roulette('option', r4);
        rouletter5.roulette('option', r5);
        rouletter6.roulette('option', r6);
        // rouletter7.roulette('option', r7);
        // rouletter8.roulette('option', r8);
    }


    $('.start').click(function () {
        confetti.stop()
        startRoulette();
        $(".winner-name").text("");
    });

    rouletter = $('div#roulette1');
    rouletter2 = $('div#roulette2');
    rouletter3 = $('div#roulette3');
    rouletter4 = $('div#roulette4');
    rouletter5 = $('div#roulette5');
    rouletter6 = $('div#roulette6');
    rouletter7 = $('div#roulette7');
    rouletter8 = $('div#roulette8');

    rouletter.roulette(r1);
    rouletter2.roulette(r2);
    rouletter3.roulette(r3);
    rouletter4.roulette(r4);
    rouletter5.roulette(r5);
    rouletter6.roulette(r6);
    rouletter7.roulette(r7);
    rouletter8.roulette(r8);

});
